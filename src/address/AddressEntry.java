package address;

public class AddressEntry {


		private int ID; 
		/**
		 * represent the first name of the contact
		 */
		private	String firstName;

		/**
		 * represent the last name of the contact
		 */
		private	String lastName;

		/**
		 * represent the Street address of the contact
		 */
		private	String street;

		/**
		 * represent the city of the contact
		 */
		private	String city;

		/**
		 * represent the State of the contact
		 */
		private	String state;

		/**
		 * represent the Zip code of the contact
		 */
		private	int zip;
		
		/**
		 * represent the Telephone number of the contact
		 */
		private	String telephone;
		
		/**
		 * represent the Email of the contact
		 */
		private	String email;

	    /**
	     *A string which will hold the value of the new line in current operation system  
	     */
		final private String newline=System.getProperty("line.separator");


		
		/**
		 *An empty Constructor for the class AddressEntry
		 */
	    AddressEntry(){}
		

	    /**
	     * class constructor with setting of all the fields
	     * @param firstname is representing the first name of the contact
	     * @param lastname is representing the last name of the contact
	     * @param street is representing the street address of the contact
	     * @param city is representing the city of the contact
	     * @param state is representing the state of the contact
	     * @param zip is representing the zip code of the contact
	     * @param email is representing the Email address of the contact
	     * @param telephone is representing the Telephone number of the contact
	     */
		AddressEntry(int ID,String firstname,String lastname,String street,String city,String state,int zip,String email,String telephone){
			this.ID=ID;
			this.firstName=firstname;
			this.lastName=lastname;
			this.street=street;
			this.city=city;
			this.state=state;
			this.zip=zip;
			this.telephone=telephone;
			this.email=email;
		}

		/**
		 *the toString method of the class is here overridden to return the contact information
		 * @return an string which contains the contact information 
		 */
		@Override  
		public String toString(){
		
			return this.ID +",   "
			+ this.firstName+" "+this.lastName +",   "
		    + this.street  + " " + this.city+", "+this.state+" "+Integer.toString(this.zip)+",   " 
		    + this.email +",   "
		    + this.telephone;
		}

}
