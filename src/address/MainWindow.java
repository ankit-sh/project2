package address;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;

import address.AddressEntry;

public class MainWindow {

	private JFrame frame;
	Vector <AddressEntry> addressEntryList = new Vector<AddressEntry>();
	JList <AddressEntry> addressEntryJList;
	DefaultListModel<AddressEntry> myaddressEntryListModel = new DefaultListModel<AddressEntry>();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		
		//make a dummy addressEntryList with 2 AddressEntry objects
				addressEntryList.add(new AddressEntry(1,"Lynne", "Grewe", "33 A street", "Hayward", "CA", 9399,"l@csueastbay.edu","555-1212"));
				addressEntryList.add(new AddressEntry(2,"Jane", "Doe", "22 Cobble street", "Hayward", "CA", 9399,"jane@csueastbay.edu","555-9999"));
				
				//because we want to REMOVE or ADD to our JList we have to create it rather than directly
				//from the Vector from a DefaultListModel (see https://docs.oracle.com/javase/tutorial/uiswing/components/list.html)
				// to which we add the elements of our vector
				
				for(int i = 0; i<addressEntryList.size(); i++)
				{  this.myaddressEntryListModel.add(i, this.addressEntryList.elementAt(i)); }
				
				
				//Now when we create our JList do it from our ListModel rather than our vector of AddressEntry
				addressEntryJList = new JList<AddressEntry>(this.myaddressEntryListModel);
				
				// create JList using the addressEntryList
				this.addressEntryJList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
				this.addressEntryJList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
				this.addressEntryJList.setVisibleRowCount(-1);
				
				
				//setup GUI and use the JList we created
				initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Address Book");
		JScrollPane scrollPane = new JScrollPane(this.addressEntryJList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		//frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		frame.getContentPane().setLayout(null);
		
		JButton btnDisplay = new JButton("Display");
		btnDisplay.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				scrollPane.revalidate();
				scrollPane.repaint();
			}
		});
		
		
		scrollPane.setBounds(10, 45, 414, 505);
		frame.getContentPane().add(scrollPane);
		btnDisplay.setBounds(69, 11, 89, 23);
		frame.getContentPane().add(btnDisplay);
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = addressEntryJList.getSelectedIndex();
				if(index != -1)//something is selected otherwise do nothing
			    //retrieve the DefaultListModel associated with our JList and remove from it the AddressEntry at this index
				((DefaultListModel<AddressEntry>) (addressEntryJList.getModel())).remove(index);
			}
		});
		btnRemove.setBounds(267, 11, 89, 23);
		frame.getContentPane().add(btnRemove);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String firstName = JOptionPane.showInputDialog(null, "First name: ");
				if (firstName == null)
					return;
				String lastName = JOptionPane.showInputDialog(null, "Last name: ");
				
			}
		});
		btnAdd.setBounds(168, 11, 89, 23);
		frame.getContentPane().add(btnAdd);
	}
}
